angular.module('app')

.directive('init', function($rootScope) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            console.log('initializing...')
            init();
        }
    }
})

// .directive('growShadow', function($interval, $timeout) {
//     return {
//         restrict: 'A',
//         link: function($scope, element, attrs) {
//             var t = function(){
//                 element.addClass('shadow');
//             }

//             $timeout(t, 100);

//         }
//     }
// })

.directive('autoheight', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {

            var setElementHeight = function() {
                var windowHeight = angular.element(window).height();
                var height = windowHeight;// > 900 ? windowHeight : 900;
                element.css('min-height', (height));
            }
            angular.element(window).on("resize", function() {
                setElementHeight();
            }).resize();

            setElementHeight();
        }
    }
})

.directive('embeddedVideo', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {

            var setElementHeight = function() {
                if (attrs.ratio) {
                    var ratio = Number(attrs.ratio);
                    var width = angular.element(element).width();
                    var height;
                    if (ratio > 1) {
                        height = width / ratio;
                    } else {
                        height = width * ratio;
                    }
                    element.css('min-height', height)

                }
            }
            angular.element(window).on("resize", function() {
                setElementHeight();
            }).resize();

            setElementHeight();
        }
    }
})

.directive('navbar', function($rootScope, $window) {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            $rootScope.isCollapsed = true;

            var nav = angular.element(element).find('.navbar-collapse');

            var toggleDisplay = function() {
                console.log('nav is visible:', nav.is(':visible'))
                console.log($rootScope.isCollapsed);

            }

            $rootScope.$watch('isCollapsed', toggleDisplay)

            var init = function init() {
                $rootScope.isMobile = $(element).find('.navbar-toggle').is(':visible');
                $rootScope.isCollapsed = $rootScope.isMobile === true;
                nav.removeClass('collapsing')
                console.log($rootScope.isCollapsed, $rootScope.isMobile);
                var isVisible = nav.is(':visible')
                console.log('visible:', isVisible)
            }

            $rootScope.toggle = function() {
                $rootScope.isCollapsed = !$rootScope.isCollapsed;
            }

            angular.element($window).bind('resize', function() {
                init();
            });

            // angular.element('a[data-scroll]').bind('click', function() {
            //     $rootScope.isCollapsed=true;
            //     toggleDisplay()
            // });

            init();
        }
    }
})


.directive('jssor', function() {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            var bodyWidth = document.body.clientWidth;
            if (bodyWidth)
                element.$SetScaleWidth(Math.min(bodyWidth, 980));
            else
                window.setTimeout(ScaleSlider, 30);


            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }
        }
    }
})

.directive('smoothScroll', function($rootScope) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {

            var height = element.height();
            var scroll = angular.element(window).scrollTop();
            if (scroll > height) {
                angular.element(".header-hide").addClass("scroll-header");
            }

            smoothScroll.init({
                speed: 1000,
                easing: 'easeInOutCubic',
                offset: '55',
                updateURL: false,
                callbackBefore: function() {
                    $rootScope.isCollapsed = true;
                    $rootScope.$apply(function() {
                        height = element.height();
                    });
                },
                callbackAfter: function() {
                    $rootScope.$apply()
                }
            });

            angular.element(window).scroll(function() {
                var height = angular.element(window).height();
                var scroll = angular.element(window).scrollTop();
                if (scroll) {
                    angular.element(".header-hide").addClass("scroll-header");
                } else {
                    angular.element(".header-hide").removeClass("scroll-header");
                }
            });
        }
    }
})


.directive('venobox', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            element.venobox({
                numeratio: true,
                infinigall: true,
                border: '20px'
            })
        }
    }
})

.directive('venoboxvid', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            element.venobox({
                bgcolor: '#000'
            })
        }
    }
})


.directive('venoboxframe', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            element.venobox({
                border: '6px'
            })
        }
    }
})

.directive('venoboxinline', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            element.venobox({
                framewidth: '300px',
                frameheight: '250px',
                border: '6px',
                bgcolor: '#f46f00'
            })
        }
    }
})

.directive('venoboxajax', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            element.venobox({
                border: '30px;',
                frameheight: '220px'
            })
        }
    }
})

.directive('gridGallery', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            new CBPGridGallery(element[0]);
        }
    }
})

.directive('tabsUi', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {
            new CBPFWTabs(element[0]);
        }
    }
})


.directive('mdTrigger', function() {
    return {
        restrict: 'C',
        link: function($scope, element, attrs) {

            var el = element[0];

            var overlay = angular.element('.md-overlay')[0];
            var modal = angular.element('#' + attrs['modal'])[0],
                close = angular.element('#' + attrs['modal']).find('.md-close')[0];

            function removeModal(hasPerspective) {
                classie.remove(modal, 'md-show');

                if (hasPerspective) {
                    classie.remove(document.documentElement, 'md-perspective');
                }
            }

            function removeModalHandler() {
                removeModal(classie.has(el, 'md-setperspective'));
            }

            el.addEventListener('click', function(ev) {
                classie.add(modal, 'md-show');
                overlay.removeEventListener('click', removeModalHandler);
                overlay.addEventListener('click', removeModalHandler);

                if (classie.has(el, 'md-setperspective')) {
                    setTimeout(function() {
                        classie.add(document.documentElement, 'md-perspective');
                    }, 25);
                }
            });

            close.addEventListener('click', function(ev) {
                ev.stopPropagation();
                removeModalHandler();
            });
        }
    }
})



// jQuery('#defaultCountdown').countdown({
//     until: new Date(2016, 1, 12)
// });
// .directive('init', function() {
//     return {
//         restrict: 'A',
//         link: function($scope, element, attrs) {
//             init();
//         }
//     }
// })
